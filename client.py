__author__ = 'Zsolt'

import sys
import socket
import random
import math
import time


class ClientApp:
    """
    The class contains the client part for the PrimeFinder's server.
    To set the host IP just edit the 1st variable in this class. (host)
    port is 50007 by default on both client and server.
    Main functions:
    connectToServer() -> connects to the server
    validateConnection() -> client authentication
    msgInterpreter() -> waits for server requests

    Request codes from server messages:
    10: request listSize and qty
    20: sending number to check for Prime
    30: not implemented yet
    90: terminate client
    """

    host = "192.168.0.5"
    port = 50007
    myRnd = random.randint(1,5000)
    codes = [10,20,30, 90]
    s = None

    def __init__(self):
        """
        The constructor creates the socket or prints an error message if failed.
        """
        try:
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error, msg:
            print "Failed to create socket. Error: %s, %s" % (str(msg[0]), msg[1])
            sys.exit()
        print "Socket created."

    def connectToServer(self):
        """
        The function connects to the server.
        The server's IP address can be changed by modifying the "host" variable
        The server's port can be changed by modifying the "port" variable
        The client's port must be the same as the server's port!
        Prints an error message if failed to connect.
        """
        try:
            self.s.connect((self.host, self.port))
            print "Connected to server."
        except socket.error, msg:
            print "Could not connect to host."

    def validateConnection(self):
        """
        The function validates the client using a random number swap technique.
        Prints an error message on fail.
        """
        try:
            self.s.sendall(str(self.myRnd))
            #print "Sent."
        except socket.error, msg:
            print "Could not send."
        try:
            #print "Incoming:"
            reply = self.s.recv(2048)
            #print reply
        except socket.error, msg:
            print "could  not receive?!"
        calc = int(reply)- self.myRnd
        self.s.sendall(str(calc))

    def sendSizeAndQty(self):
        """
        The function sends the range and the quantity entered by the user to the server.
        Prints an error message if connection is lost.
        """
        try:
            myRange = raw_input("Enter range: ")
            self.s.send(myRange)
            myQty = raw_input("Enter amount: ")
            self.s.send(myQty)
        except socket.error,msg:
            print "Connection lost."

    def checkPrime(self, number):
        """
        The function checks if a number is prime or not.
        The function returns true if the "number" argument is prime, false otherwise.
        """
        number = int(number)
        if number < 2:
            return False
        answer = True
        if number > 19:
            if (number % 2) == 0:
                return False
            sqrtOfNumber = math.sqrt(number)
            (frac, whole) = math.modf(sqrtOfNumber)
            if frac > 0:
                whole = int(whole) + 1
            counter = 2
            while counter <= whole:
                if number % counter == 0:
                    return False
                counter += 1
            return answer
        else:
            if number in [2, 3, 5, 7, 11, 13, 17, 19]:
                return True
            else:
                return False

    def sendAnswer(self, answer):
        """
        The function sends an answer to the server.
        answer can be any string
        Prints an error message if connection is lost.
        """
        try:
            self.s.send(answer)
        except socket.error,msg:
            print "Connection lost."

    def msgInterpreter(self):
        """
        The function uses an infinite loop to keep receiving messages from the server until an Exit signal is sent.
        Based on the server codes this function utilises the class's functions.(sendSizeAndQty, sendAnswer...)
        This function also records the time for the prime calculation. (in a rather ugly way)
        """
        i = 0
        while 1:
            try:
                msg = self.s.recv(2048)
                if msg:
                    code = int(msg[:2])
                    data = msg[2:]
                    print "Code: ", code
                    print "Data: " + data
                    if code in self.codes:
                        if code == 10:
                            self.sendSizeAndQty()
                        if code == 20:
                            startTime = (time.clock() * 1000)
                            isItPrime = 0
                            if self.checkPrime(data):
                                i += 1
                                isItPrime = 1
                            endTime = (time.clock() * 1000)
                            myTime = int((endTime-startTime) * 1000)
                            print "Calculation took: ", myTime, " ms"
                            self.sendAnswer("{0}{1}".format(str(isItPrime), str(myTime)))
                        if code == 90:
                            print "Number of Primes found in this session: ", i
                            print "Terminating signal received. Shutting down..."
                            self.s.close()
                            sys.exit()
            except socket.error,msg:
                print "Connection lost."
                sys.exit()


def main():
    """
    Creates an instance of the ClientApp class and runs it
    """
    app = ClientApp()
    app.connectToServer()
    app.validateConnection()
    print "Connection validated."
    app.msgInterpreter()

if __name__ == "__main__":
    main()

