__author__ = 'Zsolt'

import sys
import socket
import random
import thread


class Item:
    """
    This class is used in the Client class. Each instance of this class represents a number
    and the information related to that number.
    This class is only for storage purpose. There is no calculation in the class.
    Set and get functions are implemented for each parameter.
    number: the number to check if it is Prime
    answer: true if the number is prime, false otherwise
    ip: the IP address of the client that sent the answer
    calcTime: the time in ms of how long the prime check calculation took
    """
    number = None
    answer = None
    ip = None
    calcTime = None

    def __init__(self, num, ans, ip):
        """
        The constructor takes 3 arguments:
        num: the number to store
        ans: true if the number is prime, false otherwise
        ip: the IP address of the client that sent the answer
        """
        self.number = num
        self.answer = ans
        self.ip = ip
        self.calcTime = None

    def setAnswer(self, a):
        self.answer = a

    def setCalcTime(self, ctime):
        self.calcTime = ctime

    def getCalcTime(self):
        return self.calcTime

    def getNumber(self):
        return self.number

    def getAnswer(self):
        return self.answer


class Server:
    """
    The class handles the connections and the data storage for the application.
    ip: set to none by default
    port: the port is set in the constructor for the server
    s: the socket object
    maxConnections: maximum allowed connections
    clients: a list of clients connected to the server
    """
    ip = ''
    port = None
    s = None
    maxConnections = 5
    clients = []

    def __init__(self, port):
        """
        The constructor takes only the port as argument to create the server
        """
        self.port = port

    def start(self):
        """
        The function starts the server, creates the socket, binds it and sets the max connections.
        Prints an error message on fail.
        """
        try:
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error, msg:
            print "Unable to create socket"
            sys.exit()
        print "Socket created."
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            self.s.bind((self.ip, self.port))
        except socket.error,msg:
            print "Bind failed. Closing..."
            sys.exit()
        print "Socket bound."
        self.s.listen(self.maxConnections)

    # some basic connection validation
    def confirmConnection(self, conn, addr):
        """
        The function is a basic validation for the clients using random number swap technique.
        """
        myRnd = random.randint(1,100)
        try:
            myinput = conn.recv(2048)
        except socket.error,msg:
            print "Lost connection to client... 1"
        if myinput:
            numInput = int(myinput) + myRnd
        else:
            print "No input?... 2"
            return False
        try:
            conn.send(str(numInput))
        except socket.error,msg:
            print "Lost connection to client... Could not send data."
        try:
            myinput = conn.recv(2048)
        except socket.error,msg:
            print "Lost connection to client... 3"
        if int(myinput) == myRnd:
            return True
        else:
            return False

    def sendNumbers(self, client, conn):
        """
        The function sends the numbers from the client's list for the (client) using the (conn) connection object.
        The function either restores to send numbers or start to send a list from the beginning
        depending on client's state.
        The function receives the answer from the client and updates the corresponding client object.
        On completion the function prints some basic stats of the list and the results.
        """
        i = 0
        listLeft = client.items.__len__()
        if listLeft < client.qty:
            print "Resuming... Sending number..."
        else:
            print "Starting to send numbers to: ", client.ip
        for i in range(listLeft):
            nextNum = client.getNextNumber()
            if nextNum:
                msg = "20" + str(nextNum.getNumber())
                try:
                    conn.send(msg)
                    answer = conn.recv(2048)
                    #print answer
                    myAnswer = answer[0]
                    #print myAnswer
                    nextNum.setAnswer(int(myAnswer))
                    myTime = answer[1:]
                    #print myTime
                    # set answer and add it to the checked list
                    nextNum.setCalcTime(myTime)
                    client.addToCheckedList(nextNum)
                except socket.error,msg:
                    print "Error, connection lost..."
                    # add the number to the list again to check, if lost connection occurred
                    client.addToList(nextNum)
                    print "Cycle broke at: ", i
                    return False
                #print "Setting answer for: ", nextNum.getNumber(), " to: " + myAnswer + " from: ", client.ip
            else:
                print "Cant find next number."
        print "#######################"
        print "List size: ", listLeft
        print "Qty: ", client.qty
        checkedSize = client.checked.__len__()
        print "Checked size: ", checkedSize
        if checkedSize == client.qty:
            print "List is done. Resetting client."
            print "###############################"
            conn.send("90List done. Resetting.")
            self.resetClient(client)
        else:
            print "Something went wrong?"
            conn.send("90No more numbers?")

    # remove the client from the registered list
    def resetClient(self, client):
        """
        The function resets the client's details by removing it from the (clients) list and deleting the object.
        """
        self.clients.remove(client)
        del client

    # the function decide what to send to the client
    def msgHandler(self, client, conn):
        """
        The function decides on what to send to the client based on the client's state.
        If qty and range is set: start sending the numbers to the client.
        Otherwise request qty and range from client.
        Set qty and range and create list.
        Start sending numbers.
        """
        if client.isClientRdy():
            # send random number
            print "Sending numbers to client..."
            self.sendNumbers(client, conn)
        else:
            # get listSize and qty
            print "###########################"
            print "Need list size and qty."
            try:
                conn.send("10Security check: OK. Welcome")
                tmpRange = conn.recv(2048)
            except socket.error,msg:
                print "Error, connection lost... "
                return
            client.setListRange(int(tmpRange))
            try:
                tmpQty = conn.recv(2048)
            except socket.error,msg:
                print "Error, connection lost... "
                return
            # debug stuff
            #print tmpQty
            #print tmpRange
            client.setQty(int(tmpQty))
            # creating the list of numbers for client
            client.createList()
            # lets send these numbers
            self.sendNumbers(client, conn)


    def prepClient(self, conn, addr):
        """
        The function checks if the IP is already in the (clients) list.
        If it is a new client it adds to the (clients) list.
        If it is an existing client then it "restores the connection".
        """
        currentClient = False
        # check if client is in list
        i = 0
        cIp = str(addr[0])
        for i in range(self.clients.__len__()):
            myIp = str(self.clients[i].ip)
            if cIp == myIp:
                currentClient = self.clients[i]
                print "################################"
                print "Restoring session with client..."
                print "################################"
        if not currentClient:
            currentClient = Client(addr[0], addr[1])
            self.clients.append(currentClient)
        # let the boss decide what happens with the client
        self.msgHandler(currentClient, conn)
        self.printStats()

    def mainThread(self, conn, addr):
        """
        The function is the entry point for the threads.
        Every new connection spawns a new thread with this function.
        The thread dies if validation is failed.
        If validation succeed the connection details are sent to get registered as client. (prepClient)
        """
        if self.confirmConnection(conn, addr):
            self.prepClient(conn, addr)
        else:
            conn.close()
            print "Validation failed. Closing connection."

    def printStats(self):
        """
        The function prints stats on the server's console;
        shows: found primes with details, registered users, unique primes and unique numbers
        """
        print "###########################"
        print "List of registered clients: "
        print "###########################"
        i = 0
        # print out the list of registered clients
        for i in range(self.clients.__len__()):
            print self.clients[i].ip
        print "###########################"
        print "Found Prime numbers:"
        print "###########################"
        i = 0
        counter = 0
        # print out the found prime numbers
        for i in range(Client.foundPrimes.__len__()):
            if Client.foundPrimes[i].answer == 1:
                print Client.foundPrimes[i].number, " from: ", Client.foundPrimes[i].ip, " took: ", Client.foundPrimes[i].getCalcTime(), " ms"
                counter += 1
        print "################################################"
        print "The amount of unique Prime numbers found so far: ", counter
        print "The amount of unique numbers checked so far: ", Client.foundPrimes.__len__()
        print "################################################"

    def serveForever(self):
        """
        This function uses an infinite loop to serve the clients for as long as the script is running
        Accepts incoming connections, creates a new thread on new connection.
        """
        while 1:
            print "Listening to socket"
            (conn, addr) = self.s.accept()
            print "Connected to %s:%s" % (addr[0], addr[1])
            try:
                thread.start_new_thread(self.mainThread, (conn, addr))
            except:
                print "Can't start thread."


class Client:
    """
    The class is a representation of a client that connected to the server.
    The class stores information related to the clients:
    ip: client's ip
    port: client's port
    lastAction: not in use
    listRange: the range for the random list
    qty: the amount of random numbers to generate for the random list
    items: the list of generated random numbers
    checked: the checked numbers stored here
    foundPrimes: all the found prime numbers are stored here from all Client instances
    lastProcessedItem: a copy of the last processed item -> stored in case of connection error
    online: a flag indicating that the client is online or offline
    """
    ip = None
    port = None
    lastAction = None
    listRange = None
    qty = None
    items = []
    checked = []
    foundPrimes = []
    lastProcessedItem = None
    online = False

    def __init__(self, ip, port):
        """
        The constructor requires 2 parameters:
        ip: client's IP
        port: client's port
        Resets checked, items, qty and listRange.
        """
        self.ip = ip
        self.port = port
        self.checked = []
        self.items = []
        self.qty = None
        self.listRange = None

    def isClientRdy(self):
        """
        The function returns true if both listRange and qty is set.
        """
        if self.listRange:
            if self.qty:
                return True
        return False

    def setOnline(self, status):
        self.online = status

    def setListRange(self, listRange):
        self.listRange = listRange

    def setQty(self, qty):
        self.qty = qty

    # create the random list of numbers for this client
    def createList(self):
        """
        The function creates a list(items) of random numbers:
        The list is (qty) long
        The random numbers are in range of (1-listRange)
        """
        i = 0
        for i in range(self.qty):
            newItem = Item(random.randint(1,self.listRange), False, self.ip)
            self.items.append(newItem)
            #debug
            #print "New number added to list. IP: " + self.ip + " Number: ", newItem.number

    def getNextNumber(self):
        """
        The function returns the next item from the list(items).
        Returns false if the list is empty.
        """
        if self.items:
            current = self.items.pop()
            self.lastProcessedItem = current
            return current
        else:
            return False


    # add to list (if connection lost/no answer append it back to the list)
    def addToList(self, item):
        """
        Adds an item to the list(items).
        """
        self.items.append(item)

    def addToCheckedList(self, item):
        """
        Adds an item to the checked list (checked). Also adds the item in the (foundPrimes) list if it is a prime.
        """
        self.checked.append(item)
        i = 0
        for i in range(self.foundPrimes.__len__()):
            if self.foundPrimes[i].number == item.number:
                return
        if (item.getAnswer() == 1):
            self.foundPrimes.append(item)


def main():
    myServer = Server(50007)
    myServer.start()
    myServer.serveForever()
if __name__ == "__main__":
    main()